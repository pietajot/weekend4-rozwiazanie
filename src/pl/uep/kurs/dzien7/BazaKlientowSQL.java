package pl.uep.kurs.dzien7;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class BazaKlientowSQL implements IBazaKlientow, IWyswietlanie
{

	private Connection conn;
	private HashMap<String, Klient> klienci;
	
	public BazaKlientowSQL() throws SQLException {
		conn = BazaDanych.polaczenie();
	}

	@Override
	public int dodajKlienta(Klient klient) {
		String sql = "insert into klienci(imie, nazwisko, email) values(?, ?, ?);";
	
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, klient.getImie());
			ps.setString(2, klient.getNazwisko());
			ps.setString(3, klient.getEmail());
			ps.execute();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	
		return liczbaKlientow();
	}

	@Override
	public int usunKlienta(String email) {
		String sql = "delete from klienci where email like ?";
		
		try {
			System.out.println("Usuwam " + email);
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, email);
			ps.execute();
		} catch(SQLException e) {
		}
		return liczbaKlientow();
	}

	@Override
	public Klient getKlient(String email) {
		String sql = "select imie, nazwisko from klienci where email like ?";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, email);	
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				String imie = rs.getString("imie");
				String nazwisko = rs.getString("nazwisko");
				return new Klient(email, imie, nazwisko);
			}
			return null;
		} catch (SQLException e) {
			return null;
		}
		
	}

	@Override
	public int liczbaKlientow() {
		try {
			String sql = "select count(id) as liczba_klientow from klienci;";
			Statement zapytanie = conn.createStatement();
			ResultSet rs = zapytanie.executeQuery(sql);
			return rs.getInt("liczba_klientow");

		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public void wyswietl() {
		String sql = "select * from klienci;";
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);		
			while (rs.next()) {
				System.out.println(rs.getString("email") + " " + rs.getString("imie") + " " + rs.getString("nazwisko"));
			}
		} catch (SQLException e) {
			System.out.println("B��d po��czenia SQL");
		}
		
		
	}

}
