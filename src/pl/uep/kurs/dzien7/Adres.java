package pl.uep.kurs.dzien7;

public class Adres {
	private String ulica;
	private int numerDomu;
	private int numerLokalu;
	private String kodPocztowy;
	private String miasto;
	
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public int getNumerDomu() {
		return numerDomu;
	}
	public void setNumerDomu(int numerDomu) {
		this.numerDomu = numerDomu;
	}
	public int getNumerLokalu() {
		return numerLokalu;
	}
	public void setNumerLokalu(int numerLokalu) {
		this.numerLokalu = numerLokalu;
	}
	public String getKodPocztowy() {
		return kodPocztowy;
	}
	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}
	public String getMiasto() {
		return miasto;
	}
	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
}
