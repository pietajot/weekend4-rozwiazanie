package pl.uep.kurs.dzien7;

import java.util.ArrayList;

public class Dzial implements IWyswietlanie {
	private int dzialId;
	private String nazwa;
	private Kierownik kierownik;
	private Pracownik zastepcaKierownika;
	private ArrayList<Pracownik> pracownicy = new ArrayList<Pracownik>();
	
	public Dzial(int dzialId, String nazwa) {
		this.dzialId = dzialId;
		this.nazwa = nazwa;
	}
	
	public int dodajPracownika(Pracownik pracownik) {
		pracownicy.add(pracownik);
		
		return pracownicy.size();
	}
	
	public int usunPracownika(int index) {
		int result = -1;
		
		if ( index < pracownicy.size() ) {
			pracownicy.remove(index);
			result = 0;
		}
		return result;
	}
	
	public Pracownik getPracownikByIndex(int index) {
		Pracownik pracownik = null;
		
		if ( index < pracownicy.size() ) {
			pracownik = pracownicy.get(index);
		}
		return pracownik;
	}
	
	public Pracownik getPracownikByPesel(String pesel) {
		Pracownik pracownik = null;
		
		for ( Pracownik prac : pracownicy ) {
			if ( prac.getPesel().equals(pesel) ) {
				pracownik = prac;
				break;
			}
		}
		
		return pracownik;
	}

	public int getDzialId() {
		return dzialId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Kierownik getKierownik() {
		return kierownik;
	}

	public void setKierownik(Kierownik kierownik) {
		this.kierownik = kierownik;
	}

	public Pracownik getZastepcaKierownika() {
		return zastepcaKierownika;
	}

	public void setZastepcaKierownika(Pracownik zastepcaKierownika) {
		this.zastepcaKierownika = zastepcaKierownika;
	}

	public void wyswietl() {
		System.out.println("-----------------------");
		System.out.println("Nazwa dzia�u: " + getNazwa());
		
		System.out.println("Kierownik: ");
		kierownik.wyswietl();
		
		if ( zastepcaKierownika != null ) {
			System.out.println("Zast�pca kierownika:");
			zastepcaKierownika.wyswietl();
		}
		
		System.out.println("Pracownicy:");
		
		for ( Pracownik pracownik : pracownicy ) {
			if ( pracownik != null ) {
				pracownik.wyswietl();
			}
		}
	}
	
	public void listaPlac() {
		ArrayList<Pracownik> listaPracownikow = new ArrayList<Pracownik>();
		
		listaPracownikow.add(kierownik);
		
		if ( zastepcaKierownika != null ) {
			listaPracownikow.add(zastepcaKierownika);
		}
		
		listaPracownikow.addAll(pracownicy);
		
		System.out.println("Lista p�ac:");
		
		for ( Pracownik prac : listaPracownikow ) {
			System.out.println(" - " + prac.getImie() + " " + prac.getNazwisko() 
						+ ", wynagrodzenie = " + prac.obliczWynagrodzenie());
		}
		
	}
}



