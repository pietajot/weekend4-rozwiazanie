package pl.uep.kurs.dzien7;

public class Klient extends Osoba {
	private Adres adresDostawy;
	private String email;
	private float rabat;
	
	public Klient(String imie, String nazwisko, String email) {
		super(imie, nazwisko);
		this.email = email;
	}

	public Adres getAdresDostawy() {
		return adresDostawy;
	}

	public void setAdresDostawy(Adres adresDostawy) {
		this.adresDostawy = adresDostawy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getRabat() {
		return rabat;
	}

	public void setRabat(float rabat) {
		this.rabat = rabat;
	}
}
