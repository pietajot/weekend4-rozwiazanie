package pl.uep.kurs.dzien7;

import java.util.HashMap;
import java.util.Map;

public class BazaKlientowPamiec implements IBazaKlientow, IWyswietlanie {
	private HashMap<String, Klient> klienci = new HashMap<String, Klient>();
	
	@Override
	public void wyswietl() {
		System.out.println("Liczba klient�w: " + this.liczbaKlientow());
		
		for ( Map.Entry<String, Klient> entry : klienci.entrySet() ) {
			String key = entry.getKey();
			Klient klient = entry.getValue();
			
			System.out.println("E-mail:" + key);
			klient.wyswietl();
		}
	}

	@Override
	public int dodajKlienta(Klient klient) {
		klienci.put(klient.getEmail(), klient);
		
		return klienci.size();
	}

	@Override
	public int usunKlienta(String email) {
		klienci.remove(email);
		
		return klienci.size();
	}

	@Override
	public Klient getKlient(String email) {
		
		return klienci.get(email);
	}

	@Override
	public int liczbaKlientow() {
		
		return klienci.size();
	}

}
