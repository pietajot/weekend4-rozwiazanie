package pl.uep.kurs.dzien7;

public class Kierownik extends Pracownik {
	private double premia;
	private boolean autoSluzbowe;
	
	public Kierownik(String imie, String nazwisko, String pesel, double pensja, double premia) {
		super(imie, nazwisko, pesel, pensja);
		this.premia = premia;
	}
	
	public double obliczWynagrodzenie() {
		return getPensja() + premia;
	}
	
	public double getPremia() {
		return premia;
	}

	public void setPremia(double premia) {
		this.premia = premia;
	}

	public boolean isAutoSluzbowe() {
		return autoSluzbowe;
	}

	public void setAutoSluzbowe(boolean autoSluzbowe) {
		this.autoSluzbowe = autoSluzbowe;
	}

	public void wyswietl() {
		super.wyswietl();
		System.out.println(" Premia: " + getPremia());
	}
	
}
