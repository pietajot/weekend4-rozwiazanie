package pl.uep.kurs.dzien7;

import java.time.LocalDate;
import java.util.HashSet;

public class Pracownik extends Osoba {
	private int pracownikId;
	private String pesel;
	private String urzadSkarbowy;
	private LocalDate dataZatrudnienia;
	private double pensja;
	
	private HashSet<String> jezykiObce = new HashSet<String>();
	
	public Pracownik(String imie, String nazwisko, String pesel, double pensja) {
		super(imie, nazwisko);
		this.pesel = pesel;
		this.pensja = pensja;
	}
	
	public double obliczWynagrodzenie() {
		return pensja;
	}

	public int getPracownikId() {
		return pracownikId;
	}

	public void setPracownikId(int pracownikId) {
		this.pracownikId = pracownikId;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getUrzadSkarbowy() {
		return urzadSkarbowy;
	}

	public void setUrzadSkarbowy(String urzadSkarbowy) {
		this.urzadSkarbowy = urzadSkarbowy;
	}

	public LocalDate getDataZatrudnienia() {
		return dataZatrudnienia;
	}

	public void setDataZatrudnienia(LocalDate dataZatrudnienia) {
		this.dataZatrudnienia = dataZatrudnienia;
	}

	public double getPensja() {
		return pensja;
	}

	public void setPensja(double pensja) {
		this.pensja = pensja;
	}
	
	public void wyswietl() {
		super.wyswietl();
		System.out.println(" Pensja: " + getPensja() );
	}
	
	public void dodajJezykObcy(String jezyk) {
		jezykiObce.add(jezyk);
	}
	
	public void usunJezykObcy(String jezyk) {
		jezykiObce.remove(jezyk);
	}
	
	public boolean czyZnaJezykObcy(String jezyk) {
		return jezykiObce.contains(jezyk);
	}

	public HashSet<String> getJezykiObce() {
		return jezykiObce;
	}
	
	public void wyswietlJezykiObce() {
		for ( String jezyk : jezykiObce ) {
			System.out.println(jezyk);
		}
	}
}
