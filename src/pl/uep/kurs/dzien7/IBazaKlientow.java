package pl.uep.kurs.dzien7;

public interface IBazaKlientow {
	public int dodajKlienta(Klient klient);
	public int usunKlienta(String email);
	public Klient getKlient(String email);
	public int liczbaKlientow();
}
