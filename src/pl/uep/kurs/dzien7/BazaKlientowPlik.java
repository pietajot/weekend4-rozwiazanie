package pl.uep.kurs.dzien7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class BazaKlientowPlik implements IBazaKlientow, IWyswietlanie {

	private ArrayList<Klient> klienci;

	@Override
	public int dodajKlienta(Klient klient) {
		if (klienci == null)
			klienci = new ArrayList<Klient>();
		klienci.add(klient);
		return klienci.size();
	}

	@Override
	public int usunKlienta(String email) {
		if (klienci != null) {
			Klient k = this.getKlient(email);
			klienci.remove(k);
			return klienci.size();
		}

		return 0;
	}

	@Override
	public Klient getKlient(String email) {
		if (klienci != null) {
			for (Klient k : klienci) {
				if (k.getEmail().equals(email)) {
					return k;
				}
			}
		}
		return null;
	}

	@Override
	public int liczbaKlientow() {
		if (klienci != null) {
			return klienci.size();
		}
		return 0;
	}

	public void pobierzKlientow(String sciezka) throws IOException {
		ArrayList<Klient> listaKlientow = new ArrayList<Klient>();

		FileReader fr = new FileReader(sciezka);
		BufferedReader czytacz = new BufferedReader(fr);

		String linia = null;
		while ((linia = czytacz.readLine()) != null) {
			String[] dane = linia.split(";");
			Klient klient = new Klient(dane[0], dane[1], dane[2]);
			listaKlientow.add(klient);
		}

		czytacz.close();
		this.klienci = listaKlientow;
	}

	public ArrayList<Klient> getKlienci() {
		return this.klienci;
	}

	@Override
	public void wyswietl() {
		System.out.println("Liczba klient�w: " + this.liczbaKlientow());

		for (Klient klient : klienci) {
			System.out.println("E-mail:" + klient.getEmail());
			klient.wyswietl();
		}
	}

}
