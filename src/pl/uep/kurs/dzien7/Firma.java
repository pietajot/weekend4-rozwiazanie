package pl.uep.kurs.dzien7;

import java.util.ArrayList;
import java.util.Iterator;

public class Firma implements IWyswietlanie, IAdresowanie {
	private String nazwa;
	private Adres adres;
	
	private ArrayList<Dzial> dzialy = new ArrayList<Dzial>();

	private IBazaKlientow klienci;
	
	public IBazaKlientow getKlienci() {
		return klienci;
	}

	public void setKlienci(IBazaKlientow klienci) {
		this.klienci = klienci;
	}

	public ArrayList<Dzial> getDzialy() {
		return dzialy;
	}

	public Firma(String nazwa) {
		this.nazwa = nazwa;
	}
	
	public int dodajDzial(Dzial dzial) {
		dzialy.add(dzial);
		
		return dzialy.size();
	}
	
	public int usunDzial(int index) {
		int result = -1;
		
		if ( index < dzialy.size() ) {
			dzialy.remove(index);
			result = dzialy.size();
		}
		return result;
	}
	
	public Dzial getDzialByIndex(int index) {
		Dzial dzial = null;
		
		if ( index < dzialy.size() ) {
			dzial = dzialy.get(index);
		}
		return dzial;
	}
	
	public Dzial getDzialById(int dzialId) {
		Dzial dzial = null;
		
		Iterator<Dzial> it = dzialy.iterator();
		
		while ( it.hasNext() ) {
			Dzial iDzial = it.next();
			if ( iDzial.getDzialId() == dzialId ) {
				dzial = iDzial;
				break;
			}
		}
		return dzial;
	}
	
	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	@Override
	public Adres getAdres() {
		return adres;
	}

	@Override
	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	@Override
	public void wyswietl() {
		System.out.println("Nazwa firmy: " + this.getNazwa());
		
		System.out.println("Liczba dzia��w: " + this.getDzialy().size());
		for ( Dzial dzial : dzialy ) {
			dzial.wyswietl();
		}
		
		if ( klienci != null ) {
			System.out.println("-----Baza klient�w-----");
			((IWyswietlanie)klienci).wyswietl();
		}
	}
}
