package pl.uep.kurs.dzien7;

public interface IAdresowanie {
	public Adres getAdres();
	public void setAdres(Adres adres);
}
